<!-- @extends('layouts.app') -->
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @if($data['formType']=='create')
                <h1 class="page-header">Assign Custody</h1>
                @csrf
                @endif
                @if($data['formType']=='edit')
                <h1 class="page-header">Edit Custody</h1>
                @csrf
                @endif
                @if($data['formType']=='show')
                <h1 class="page-header">View Custody</h1>
                @csrf
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse">Custody Details</a>
                            </h4>
                        </div>
                        <div id="addnewvehicle" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <!-- for display form validation errors -->
                                @if ($errors->any())
                                <script> $("#addnewuser").addClass('in');</script>
                                <div class="alert alert-danger">
                                    <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                    </ul>
                                </div>
                                @endif
                                <!-- end display form validation errors -->
                                @if($data['formType']=='create')
                                <form role="form" action="{{route('custody-management.store')}}" id="custodyAddForm" name="custodyAddForm" method="POST" class="row">
                                @csrf
                                @endif
                                @if($data['formType']=='edit')
                                <form role="form" action="{{route('custody-management.update',$data['custody'][0]['id'])}}" id="custodyAddForm" name="custodyAddForm" method="POST" class="row">
                                @method('PUT')
                                @csrf
                                @endif
                                @if($data['formType']=='show')
                                <form role="form" id="custodyAddForm" name="custodyAddForm" class="row">
                                @csrf
                                @endif
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <input type="text" name="register_number" id="register_number" class="form-control" value="{{old('register_number',$data['custody'][0]['register_number'] ?? '')}}" {{$data['formType']=='show'?'disabled':''}}  placeholder="VEHICLE REGISTER NUMBER">
                                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <select name="region" id="region" class="form-control">
                                                <option value="-1">Please Select Region</option>
                                                @foreach ($data['region'] as $region)

                                                <option value="{{$region['region_id']}}" {{(isset($data['custody'][0]['region']) && $data['custody'][0]['region']==$region['region_id'])?'selected':''}}>{{$region['region']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                        <select type="text" name="district" id="district" class="form-control" placeholder="Select district">
                                            <option value="">Select District</option>
                                            @if(isset($data['custody'][0]['district_name']) && $data['custody'][0]['district_name']!=null)
                                            <option value="{{$data['custody'][0]['district_id']}}" selected>{{$data['custody'][0]['district_name']}}</option>
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                        <select type="text" name="office" id="office" class="form-control" placeholder="Select office">
                                        <option value="">Select Office</option>
                                            @if(isset($data['custody'][0]['office_name']) && $data['custody'][0]['office_name']!=null)
                                            <option value="{{$data['custody'][0]['office_id']}}" selected>{{$data['custody'][0]['office_name']}} </option>
                                            @endif
                                        </select>
                                        </div>
                                    </div>
                                    

                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            @if($data['formType']!='show')
                                            <button type="submit" class="btn btn-default">Save</button>
                                            @endif
                                        <div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- </div> -->
                    </div>
                </div>   
            </div> 
        </div>
    </div>
</div>
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                    responsive: true
            });
            
        });

        $(document).on('change','#region',function(){
            var region_id   =   $(this).val(); 
            GetDistrict(region_id);
            ///alert($('#district').val());
            // if(officerLevel==4){
            // GetOffice($('#district').val());
            // }
        });
        $(document).on('change','#district',function(){
            var did=$(this).val(); 
            //alert(officerLevel);
            GetOffice(did);
        });

        function GetDistrict(did){
            var url="{{route('GetDistrict')}}";
            $.ajax({
                url:url,
                type:'GET',
                data : {did:did},
                success: function(res) {
                    $("#district").html(res);
                    $("#district").css('display','block');
                }
            });
        }

        function GetOffice(oid){
            var url="{{route('GetOffice')}}";
            $.ajax({
                url:url,
                type:'GET',
                data : {oid:oid},
                success: function(res) {
                    $("#office").html(res);
                    $("#office").css('display','block');
                }
            });
        }
    </script>
@endsection